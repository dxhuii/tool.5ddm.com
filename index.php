<?php if($_REQUEST["p"] == "611"){ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<title>工具</title>
</head>
<style>
	*{margin: 0;padding:0;box-sizing: border-box;}
	input,textarea,button{outline: none;}
	ul,li{list-style-type: none;}
	.tool{margin: 5px 30px;min-width: 600px;}
	.tool input[type="text"], .tool select{width: 92%;height:36px;line-height: 36px;border:1px solid #ddd;font-size:18px;padding: 0 10px;border-radius: 5px;}
	.tool div{margin-top: 10px;line-height: 38px;color: #333;}
	.tool span{width: 8%;float:left;}
	.tool textarea{margin-left: 8%;width: 92%;height:366px;border: 1px solid #ddd;border-radius: 5px;padding: 10px;line-height:28px;font-size: 16px;}
	.tool button{margin-left: 8%;width: 92%;height:36px;line-height: 36px;background-color: #f60;border-radius: 5px;color: #fff;font-size: 18px;text-align: center;border: none;cursor: pointer;}
	.kan{display: none;}
</style>
<body>
<div class="tool">
	<div class="radio">
		<span>网站：</span>
		<label><input type="radio" name="site" value="bilibili" checked /> B站</label>
		<label><input type="radio" name="site" value="iqiyi" /> 爱奇艺</label>
		<label><input type="radio" name="site" value="youku" /> 优酷</label>
		<label><input type="radio" name="site" value="qq" /> 腾讯</label>
		<label><input type="radio" name="site" value="sohu" /> 搜狐</label>
		<label><input type="radio" name="site" value="acfun" /> A站</label>
		<label><input type="radio" name="site" value="le" /> 乐视</label>
		<label><input type="radio" name="site" value="pptv" /> PPTV</label>
		<label><input type="radio" name="site" value="mgtv" /> 芒果TV</label>
		<label><input type="radio" name="site" value="migu" /> 咪咕</label>
		<label><input type="radio" name="site" value="kan" /> 360KAN</label>
		<label><input type="radio" name="site" value="douban" /> 豆瓣</label>
		<label><input type="radio" name="site" value="baike" /> 百科</label>
	</div>
	<div class="radio">
		<span>网站：</span>
		<label><input type="radio" name="type" value="url" checked /> 链接</label>
		<label><input type="radio" name="type" value="ep" /> 剧情</label>
	</div>
	<div><span>数据URL：</span><input type="text" name="data" /></div>
	<div><span>专辑ID：</span><input type="text" name="id" /></div>
	<div class="kan">
		<div><span>综艺：</span><input type="text" name="year"></div>
		<div><span>网站：</span>
			<select type="text" name="site">
				<option value='qq' selected>腾讯</option>
				<option value='youku'>优酷</option>
				<option value='levp'>乐视(levp)</option>
				<option value='leshi'>乐视(leshi)</option>
				<option value='sohu'>搜狐</option>
				<option value='qiyi'>爱奇艺</option>
				<option value='imgo'>芒果TV</option>
				<option value='bilibili'>哔哩哔哩</option>
				<option value='fengxing'>风行</option>
				<option value='huashu'>华硕TV</option>
				<option value='pptv'>聚力(PPTV)</option>
				<option value='acfun'>A站</option>
				<option value='xunlei'>迅雷</option>
				<option value='cntv'>cntv</option>
			</select>
		</div>
		<div>
			<span>分类：</span>
			<select name="cate">
				<option value='1'>电影</option>
				<option value='2'>电视</option>
				<option value='3'>综艺</option>
				<option value='4' selected>动漫</option>
			</select>
		</div>
		<div>
			<span>链接：</span>
			<select name="href">
				<option value='0'>要A标签</option>
				<option value='1'>不要A标签</option>
				<option value='2' selected>需要序号</option>
			</select>
		</div>
	</div>
	<div><textarea name="content"></textarea></>
	<button type="button">出来</button>
</div>
<script src="//s0.pstatp.com/cdn/expire-1-M/jquery/3.4.0/jquery.min.js"></script>
<script type="module">
	const obj = {
		youku: 'https://list.youku.com/show/id_cc001f06962411de83b1.html',
		kan: 'http://www.360kan.com/cover/switchsite?site=qq&id=RLltbH7kTG0tMn&category=2',
		iqiyi: 'https://pcw-api.iqiyi.com/albums/album/avlistinfo?aid=202861101',
		pptv: 'http://apis.web.pptv.com/show/videoList?from=web&version=1.0.0&format=jsonp&pid=9040561',
		sohu: 'http://pl.hd.sohu.com/videolist?playlistid=1007177',
		bilibili: 'https://api.bilibili.com/pgc/web/season/section?season_id=5398',
		le: 'http://d.api.m.le.com/detail/episode?pid=10052877&platform=pc&page=1&pagesize=1000000000',
		acfun: 'https://www.acfun.cn/bangumi/aa6002598?quickViewId=pagelet_partlist&reqID=6&ajaxpipe=1&t=1598095825709',
		mgtv: 'https://pcweb.api.mgtv.com/episode/list?collection_id=322351&page=1&size=50',
		qq: 'https://v.qq.com/x/cover/m441e3rjq9kwpsc/k0034skpb74.html',
		baike: decodeURIComponent('https://baike.baidu.com/item/%E6%96%A9%EF%BC%81%E8%B5%A4%E7%BA%A2%E4%B9%8B%E7%9E%B3/18149129'),
		douban: 'https://ptgen.rhilip.workers.dev/|https://api.douban.workers.dev/|https://pf.5ddm.workers.dev/,https://pf.5ddm.com/',
		migu: 'https://webapi.miguvideo.com/gateway/program/v3/cont/content-info/659983799/1'
	};

	const style = (data, type, vip, i, cate) => {
		var f = data.split('?')[0];
		var isNum = type === 2 ? ('第' + (i < 10 ? '0' + i : i) + (cate === 4 ? '话' : '集') + '$') : '';
		return type ? isNum + f : '<a href="'+ f + '">'+ (vip ? '<i class="vip"></i>' : '') +'</a>'
	}

	const trim = str => {
		return str.replace(/(^\s*)|(\s*$)/g, '')
	}

	const bilibili = async (id, type) => {
		let pid = id;
		if(id.indexOf('md') !== -1) {
			const md = await $.getJSON(`./mdbili.php?id=${id.replace(/[^0-9]/ig,'')}`);
			pid = md.result.media.season_id;
		}
		const res = await $.getJSON(`./bili.php?id=${pid}`);
		const { episodes = [] } = res.result.main_section
		const html = episodes.map(({title, long_title, share_url}) => {
			const isbr = parseInt(title) > (episodes.length - 1)
			if(type === 'ep') {
				return `第${title}话@@${long_title}@@暂无内容${isbr ? '' : '||\n'}`;
			} else {
				return `第${title}话${long_title ? ` ${long_title}$` : '$'}${share_url.replace('http:', '')}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const iqiyi = async (id, type) => {
		const res = await $.getJSON(`./iqiyi.php?id=${id}`);
		const { epsodelist = [] } = res.data
		const html = epsodelist.map(({order, subtitle, playUrl}) => {
			const isbr = parseInt(order) > (epsodelist.length - 1)
			if(type === 'ep') {
				return `第${order}话@@${subtitle}@@暂无内容${isbr ? '' : '||\n'}`;
			} else {
				return `第${order}话${subtitle ? ` ${subtitle}$` : '$'}${playUrl.replace('http:', '')}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const sohu = async (id, type) => {
		const res = await $.getJSON(`./sohu.php?id=${id}`);
		console.log(res)
		const { videos = [] } = res
		const html = videos.map(({order, videoDesc, pageUrl}) => {
			const isbr = parseInt(order) > (videos.length - 1)
			if(type === 'ep') {
				return `第${order}话@@@@${videoDesc ? videoDesc : '暂无内容'}${isbr ? '' : '||\n'}`;
			} else {
				return `第${order}话$${pageUrl}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const pptv = async (id, type) => {
		const res = await $.getJSON(`./pptv.php?id=${id}`);
		console.log(res)
		const { list = [] } = res.data
		const html = list.map(({epTitle, url}) => {
			const isbr = parseInt(epTitle) > (list.length - 1)
			if(type === 'ep') {
				return `第${epTitle}话@@@@${isbr ? '' : '||\n'}`;
			} else {
				return `第${epTitle}话$${url}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const le = async (id, type) => {
		const res = await $.getJSON(`./le.php?id=${id}`);
		console.log(res)
		const { list = [] } = res.data
		const html = list.map(({episode, sub_title, vid, description}) => {
			const isbr = parseInt(episode) > (list.length - 1)
			if(type === 'ep') {
				return `第${episode}话@@${sub_title}@@${description ? description : '暂无内容'}${isbr ? '' : '||\n'}`;
			} else {
				return `第${episode}话${sub_title ? ` ${sub_title}$` : '$'}//www.le.com/ptv/vplay/${vid}.html${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const kan = async (id, year, site, cate, type) => {
		var html = '';
		const res = await $.getJSON('./360.php?site='+ site +'&id='+ id + (cate === 3 ? '&year=' + year : '&category=' + cate))
		console.log(res)
		var item = res.data;
		var box = cate === 3 ? $(item).find('#js-year-all') : $(item).find('.num-tab-main');
		var isMore = cate === 3 || box.length <= 1;
		var html = '';
		for(var k = (isMore ? 0 : 1); k < box.length; k++){
			var bb = box.eq(k).find('a');
			var len = !isMore ? bb.length - 1 : bb.length;
			for(var i = 0; i < len; i++){
				var href = bb.eq(i).attr('href');
				var isVip = bb.eq(i).find('.ico-fufei').length;
				html += (i > (len - 2) && k === box.length - 1) ? style(href, type, isVip, (i+1+parseInt(!isMore ? (k - 1)*100 : 0)), cate) : style(href, type, isVip, (i+1+parseInt(!isMore ? (k - 1)*100 : 0)), cate) + '\n';
			}
		}
		return html;
	}

	const acfun = async (id, type) => {
		let data = ''
		const res = await $.getJSON(`./acfun.php?id=${id}`).catch((e) => {
			data = e.responseText
		});
		const list = $(data).find('.single-p')
		const html = list.map((i, item) => {
			const { index, href } = item.dataset
			const title = $(item).text().split(' ');
			const subTitle = title[1];
			const isbr = parseInt(index) > (list.length - 2)
			if(type === 'ep') {
				return `${title[0]}@@${subTitle}@@'暂无内容'${isbr ? '' : '||\n'}`;
			} else {
				return `${title[0]}${subTitle ? ` ${subTitle}$` : '$'}https://www.acfun.cn${href}${isbr ? '' : '\n'}`;
			}
		})
		return [...html].join('')
	}

	const youku = async (id, type) => {
		let data = ''
		const res = await $.getJSON(`./youku.php?id=${id}${type ? `&type=${type}` : ''}`).catch((e) => {
			data = e.responseText
			console.log(data)
		});
		const arr = data.split(type === 'ep' ? '||' : '@')
		const html = arr.map((item, i) => {
			const isbr = i > (arr.length - 3)
			if(type === 'ep') {
				return `${item}${isbr ? '' : '||\n'}`;
			} else {
				return `${item}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const mgtv = async (id, type) => {
		let data = ''
		let arrId = id.split(',')
		const res = await $.getJSON(`./mgtv.php?id=${arrId[0]}&page=${arrId[1]}`)
		console.log(res)
		const { list = [] } = res.data
		const html = list.map(({t1, t2, url}) => {
			const isbr = parseInt(t1) > (list.length - 1)
			if(type === 'ep') {
				return `第${t1}话@@${t2}@@暂无内容${isbr ? '' : '||\n'}`;
			} else {
				return `第${t1}话${t2 ? ` ${t2}$` : '$'}//www.mgtv.com${url}.html${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const migu = async (id, type) => {
		let data = ''
		const res = await $.getJSON(`./migu.php?id=${id}`).catch((e) => {
			data = JSON.parse(e.responseText.replace('<!--  -->', '').trim())
		});
		console.log(data)
		const { datas = [] } = data.body.data
		const html = datas.map(({index, detail, pID}) => {
			const isbr = parseInt(index) > datas.length
			if(type === 'ep') {
				return `第${index}话@@@@${detail}${isbr ? '' : '||\n'}`;
			} else {
				return `第${index}话$//www.miguvideo.com/mgs/website/prd/detail.html?cid=${pID}${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const qq = async (id, type) => {
		let data = ''
		let arrId = id.split(',')
		const res = await $.getJSON(`./qq.php?id=${id}`)
		const pid = res[0].id
		const list = JSON.parse(`[${res[0].list}]`)
		console.log(list)
		const html = list.map(({V, E}) => {
			const isbr = parseInt(E) > (list.length - 1)
			if(type === 'ep') {
				return `第${E}话@@@@暂无内容${isbr ? '' : '||\n'}`;
			} else {
				return `第${E}话$//v.qq.com/x/cover/${pid}/${V}.html${isbr ? '' : '\n'}`;
			}
		})
		return html.join('');
	}

	const douban = async (id, type) => {
		// const res = await $.getJSON(`./douban.php?id=${id}`)
		const res = await $.getJSON(`https://pf.5ddm.workers.dev/?url=${id}`)
		console.log(res)
		return JSON.stringify(res);
	}

	const baike = async (id, type) => {
		let data = ''
		const res = await $.getJSON(`./baike.php?url=${id}`).catch((e) => {
			data = e.responseText
			console.log(data)
		});

		const dt = $(data).find('dt')
		const dd = $(data).find('dd')

		const html = dt.map((i, item) => {
			const isbr = i > (dt.length - 2)
			const title = $(item).text().split(' ')
			const des = trim($(dd).eq(i).text().replace('剧情图片', ''))
			return title.length > 1 ? `${title[0]}@@${title[1]}@@${des}${isbr ? '' : '||\n'}` : `${title[0]}@@@@${des}${isbr ? '' : '||\n'}`
		})

		return [...html].join('');
	}
	
	const data = document.querySelector('input[name="data"]')
	const site = document.querySelector('input[name="site"]:checked').value;
	data.value = obj[site]
	document.querySelector('.radio').addEventListener('click', (e) => {
		data.value = obj[e.target.value];
		if(e.target.value === 'kan') {
			$('.kan').show();
		} else {
			$('.kan').hide();
		}
	})
	document.querySelector('button').addEventListener('click', async (e) => {
		const id = document.querySelector('input[name="id"]').value;
		const type = document.querySelector('input[name="type"]:checked').value;
		const site = document.querySelector('input[name="site"]:checked').value;
		data.value = obj[site];
		let html = '';
		switch (site) {
			case 'bilibili':
				html = await bilibili(id, type)
				break;
			case 'iqiyi':
				html = await iqiyi(id, type)
				break;
			case 'sohu':
				html = await sohu(id, type)
				break;
			case 'pptv':
				html = await pptv(id, type)
				break;
			case 'le':
				html = await le(id, type)
				break;
			case 'acfun':
				html = await acfun(id, type)
				break;
			case 'youku':
				html = await youku(id, type)
				break;
			case 'mgtv':
				html = await mgtv(id, type)
				break;
			case 'qq':
				html = await qq(id, type)
				break;
			case 'douban':
				html = await douban(id, type)
				break;
			case 'baike':
				html = await baike(id, type)
				break;
			case 'migu':
				html = await migu(id, type)
				break;
			case 'kan':
				var year = $('input[name="year"]').val();
				var site2 = $('select[name="site"] option:selected').val();
				var cate = parseInt($('select[name="cate"] option:selected').val());
				var href = parseInt($('select[name="href"] option:selected').val());
				html = await kan(id, year, site2, cate, href)
				break;
		}
		document.querySelector('textarea[name="content"]').value = html;
	})
</script>
</body>
</html>
<?php }else{ ?>
<h2>你没有权限</h2>
<?php } ?>