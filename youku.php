<?php
require './curl.php';
$id = $_REQUEST["id"];
$type = $_REQUEST["type"];

$replace= array('window.jQuery && jQuery(',');'); //要替换掉的字符，把获取的数据变化为json格式
$html = curl_get_contents("https://list.youku.com/show/{$id}.html"); //采集剧集页面
preg_match('/showid:"(.*)"/U', $html, $showid); //获取分类名称、showid
$str = curl_get_contents("https://list.youku.com/show/module?id=".$showid[1]."&tab=showInfo&callback=jQuery");
$con = json_decode(str_ireplace($replace,'',$str)); //采集剧集页面分页参数json
preg_match_all('/<li(.*?)data-id\="(.+?)\">/is',$con->html, $data_id); //匹配剧集分页参数
$data_id[2] = empty($data_id[2]) ? array("reload_1") : $data_id[2]; //当剧集分页参数为空指定一个"reload_1"为第一页,并且为数组,否则为获取到的剧集分页参数
$arr = array();
foreach($data_id[2] as $key => $getid) {
    $arr_con = json_decode(str_ireplace($replace,'', curl_get_contents("https://list.youku.com/show/point?id={$showid[1]}&stage={$getid}&callback=jQuery"))); //获取剧集json数据
    preg_match_all('/<div class=\"p-item-info\"><div class=\"item-title c555\"><a href=\"(.+?)" title=\"(.+?)" target=\"video\">(.+?)<\/a><\/div><div class=\"item-intro c999\">(.+?)<\/div><\/div>/is',$arr_con->html, $new_con); //匹配剧集、集数名称和播放页地址
    $list =  array_combine($new_con[2], $new_con[1]); //合并集数名称和播放页地址
    $title = $new_con[2];
    $url = $new_con[1];
    $des = $new_con[4];
    $i = 1 + $key * 10;
    foreach($title as $key => $v){
        $u = preg_replace('/\?(.*?)/iU','', $url[$key]);
        if($type == 'ep') {
            echo '第'.$i.'集@@'.$v.'@@'.$des[$key].'||';
        } else {
            echo '第'.$i.'集 '.$v.'$'.$u.'@';
        }
        $i++;
    }
    // if($type == 'ep') {
    //     $arr_con = json_decode(str_ireplace($replace,'', curl_get_contents("https://list.youku.com/show/point?id={$showid[1]}&stage={$getid}&callback=jQuery"))); //获取剧集json数据
    //     preg_match_all('/<div class=\"p-item-info\"><div class=\"item-title c555\"><a href=\"(.+?)" title=\"(.+?)" target=\"video\">(.+?)<\/a><\/div><div class=\"item-intro c999\">(.+?)<\/div><\/div>/is',$arr_con->html, $new_con); //匹配剧集、集数名称和播放页地址
    //     $list =  array_combine($new_con[2], $new_con[1]); //合并集数名称和播放页地址
    //     $title = $new_con[2];
    //     $url = $new_con[1];
    //     $des = $new_con[4];
    //     $i = 1 + $key * 10;
    //     foreach($title as $key => $v){
    //         $v = preg_replace('/\?(.*?)/iU','',$v);
    //         $a = array('title' => '第'.$i.'集 '.$v, 'url' => $url[$key], 'des' => $des[$key]);
    //         array_push($arr, $a);
    //         // echo '第'.$i.'集 '.$id.'$'.$v.'@';
    //         $i++;
    //     }
    //     echo json_encode($arr);
    // } else {
    //     $arr_con = json_decode(str_ireplace($replace,'', curl_get_contents("https://list.youku.com/show/episode?id={$showid[1]}&stage={$getid}&callback=jQuery"))); //获取剧集json数据
    //     preg_match_all('/href=\"(.+?)" target=\"_blank\">(.+?)<\/a>/is',$arr_con->html, $new_con); //匹配剧集、集数名称和播放页地址
    //     $list =  array_combine($new_con[2], $new_con[1]); //合并集数名称和播放页地址
    //     $i = 1 + $key * 10;
    //     foreach($list as $id => $v){
    //         $v = preg_replace('/\?(.*?)/iU','',$v);
    //         // $a = array('title' => '第'.$i.'集 '.$id, 'url' => $v);
    //         // array_push($arr, $a);
    //         echo '第'.$i.'集 '.$id.'$'.$v.'@';
    //         $i++;
    //     }
    // }
}

// echo json_encode(["data" => $arr]);

?>