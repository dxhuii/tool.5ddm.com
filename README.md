# 工具集

## 优酷

```html
https://list.youku.com/show/id_cc001f06962411de83b1.html
https://list.youku.com/show/episode?id=19461&stage=reload_21&callback=j
```
### 例子 19461
19461 | 1    
------|------
ID    | 第一页

## 优酷剧情，除动漫外链接

```html
https://list.youku.com/show/point?id=参数1&stage=reload_参数2&callback=y
https://list.youku.com/show/point?id=19461&stage=reload_11&callback=jQuery
https://list.youku.com/show/module?id=19461&tab=point&callback=jQuery
```
### 例子 19461,1

19461 | 1
------|----------------
ID    | 剧情&&第几页数开始

## 360KAN

```text
http://www.360kan.com/cover/switchsite?site=参数1&id=参数2&category=参数3
http://www.360kan.com/cover/zongyilist?id=参数1&site=参数2&do=switchyear&year=参数4
```
### 根据页面提示选择，如果是综艺，在综艺那一栏填入年份即可

## 爱奇艺

```html
http://cache.video.qiyi.com/jp/avlist/202861101/1/50/
https://pcw-api.iqiyi.com/albums/album/avlistinfo?aid={$id}&size=5000&page=1
```
### 例子 202861101/1/50/ 新版接口可以只填写id

202861101 | 1     | 50      
----------|-------|---------
ID        | 页数   | 固定参数 

## PPTV

```html
http://apis.web.pptv.com/show/videoList?from=web&version=1.0.0&format=jsonp&pid=参数1
```

### 例子 9040561

9040561 |       
--------|
ID      |

- JSON是指，不能获取成功时，把参数拼到上面链接，打开，获取到JSON后，写入文本区域，点提交。

## 乐视

```html
http://d.api.m.le.com/detail/episode?pid=参数1&platform=pc&page=1&pagesize=1000000000
```

### 例子 10024412

10024412|
--------|
ID      |

## 搜狐

```html
http://pl.hd.sohu.com/videolist?playlistid=参数1
```

### 例子 1007177

1007177 |
--------|
ID      |

## B站

```html
https://www.bilibili.com/bangumi/media/md28223043/
https://bangumi.bilibili.com/view/web_api/season?season_id=5398
https://api.bilibili.com/pgc/web/season/section?season_id=27959
```
### 例子 1007177

5615    |
--------|
ID      |

## 芒果TV
```html
https://pcweb.api.mgtv.com/episode/list?collection_id=322351&page=1&size=50
https://pcweb.api.mgtv.com/episode/list?video_id=4285061&page=1&size=50
```

322351          |4285061
----------------|--------
collection_id   |video_id

## 腾讯视频
```html
https://v.qq.com/x/cover/m441e3rjq9kwpsc/k0034skpb74.html
```